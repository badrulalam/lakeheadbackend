/**
 * Created by ash on 6/14/15.
 */
angular.module('app')
    .controller('AdminEditEventController', function($scope, $stateParams, uiGridConstants, $http,$state,Options) {

        $scope.alerts =[];
        $scope.action='Update';

        $scope.title="Edit Event";

        $scope.event={}
        $scope.event.name = "";
        $scope.event.details = "";
        $scope.event.evtdate = "";
        $scope.event.slug = "";





        console.log($stateParams);


        $http.get('/event/'+ $stateParams.evtid).success (function(data){

            console.log(data);
            $scope.event={}

            $scope.event.name=data.name;
            $scope.event.details =data.details;

            var d = new Date(data.evtdate);
            //var res =d.getDate() + '/' + (d.getMonth()+1) + '/' + d.getFullYear();
            var eday = d.getDate();
            var emon = d.getMonth()+1;
            var ey = d.getFullYear();

            var fd =(eday<10?'0':'')+ eday ;
            var fm =(emon<10?'0':'')+ emon ;



            var res =fd + '/' + fm + '/' + d.getFullYear();
            //$scope.event.evtdate = res;
            $scope.dt = res;
            console.log('Event date',$scope.dt);
            $scope.event.slug = data.slug;

        })





        $scope.makeNameSlug = function(){
            $scope.event.slug = Options.convertToSlug( $scope.event.name);
        }

        $scope.saveEvent = function(){
            //var queryString = 'category_title='+$scope.blog.category_title+'&category_slug='+$scope.blog.category_slug+'&category_desc='+$scope.blog.category_desc;

            $scope.event.evtdate = new Date($scope.dt).getTime();
            var queryString = {"name":$scope.event.name,"details": $scope.event.details,"evtdate": $scope.event.evtdate,"slug": $scope.event.slug};

            $http.post('/event/'+$stateParams.evtid,queryString).

                success(function(data, status, headers, config) {
                    // this callback will be called asynchronously
                    // when the response is available
                    console.log("Event record Updated");
                    console.log(data);

                })

        }


        //------Datepeaker Start------------------

        //datepicker
        $scope.today = function() {
            $scope.dt = new Date();
        };
        //$scope.today();
        $scope.clear = function () {
            $scope.dt = null;
        };
        // Disable weekend selection
        $scope.disabled = function(date, mode) {
            return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
        };
        $scope.toggleMin = function() {
            $scope.minDate = $scope.minDate ? null : new Date();
        };
        $scope.toggleMin();
        $scope.open = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.opened = true;
        };
        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1,
            format: 'dd-mm-yyyy'
        };
        $scope.formats = ['dd-MMMM-yyyy','yyyy/MM/dd','dd.MM.yyyy','dd/MM/yyyy','shortDate'];
        $scope.format = $scope.formats[3];



        //-----Datepeaker End--------------------
    });
