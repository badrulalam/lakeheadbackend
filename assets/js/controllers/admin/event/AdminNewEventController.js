angular.module('app')
    .controller('AdminNewEventControlle', function($scope,$http,$state,Options,$upload) {

        $scope.alerts =[];
        $scope.action='Add';

        $scope.title="Add New Event";

        $scope.event={}
        $scope.event.name = "";
        $scope.event.details = "";
        $scope.event.evtdate = "";
        $scope.event.slug = "";

        //$scope.event.make_date = "";



        $scope.makeNameSlug = function(){
            $scope.event.slug = Options.convertToSlug( $scope.event.name);
        };

        $scope.upload = function (files) {
            $scope.files = files;
        };



        $scope.saveEvent = function(){
            $scope.event.evtdate = new Date($scope.dt).getTime();

            if ($scope.files && $scope.files.length) {
                for (var i = 0; i < $scope.files.length; i++) {
                    var file = $scope.files[i];
                    console.log("take file");
                    console.log($upload);
                    $upload.upload({
                        url: '/event/upload',
                        fields: $scope.event,
                        file: file
                    }).progress(function (evt) {
                        //  $("#profile-img-cont").empty().html('<i class="fa fa-refresh fa-spin"></i>');
                        var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                        console.log('progress: ' + progressPercentage + '% ' +
                            evt.config.file.name);
                    }).success(function (data, status, headers, config) {
                        console.log('filename: ' + config.file.name + ' uploaded. Response: ' +
                            JSON.stringify(data));
                        $scope.image_path=data.data.image;
                        $scope.alerts.push({ type: 'success', msg: 'Well done! You have successfully created a post( '+data.data.name+' ).' });
                    });
                }
            }
        };
        //
        //$scope.saveEvent_old = function(){
        //    //var queryString = 'category_title='+$scope.blog.category_title+'&category_slug='+$scope.blog.category_slug+'&category_desc='+$scope.blog.category_desc;
        //
        //
        //        $scope.event.evtdate = new Date($scope.dt).getTime();
        //
        //
        //
        //
        //    var queryString = {"name":$scope.event.name,"details": $scope.event.details,"evtdate":  $scope.event.evtdate,"slug": $scope.event.slug};
        //
        //    $http.post("/event/create",queryString).
        //        //$http.post("/category/create?"+queryString).
        //        success(function(data, status, headers, config) {
        //            // this callback will be called asynchronously
        //            // when the response is available
        //            console.log("Event record created")
        //            console.log(data);
        //            $scope.alerts.push({ type: 'success', msg: 'Well done! You have successfully created a category( '+data.name+' ).' });
        //
        //        }).
        //        error(function(data, status, headers, config) {
        //            $scope.alerts.push({ type: 'danger', msg: 'Oh snap! Change a few things up and try submitting again.' });
        //        });
        //}

        $scope.addAlert = function() {
            $scope.alerts.push({msg: 'Another alert!'});
        };

        $scope.closeAlert = function(index) {
            $scope.alerts.splice(index, 1);
        };




        //------Datepeaker Start------------------

        //datepicker
        $scope.today = function() {
            $scope.dt = new Date();
        };
        //$scope.today();
        $scope.clear = function () {
            $scope.dt = null;
        };
        // Disable weekend selection
        $scope.disabled = function(date, mode) {
            return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
        };
        $scope.toggleMin = function() {
            $scope.minDate = $scope.minDate ? null : new Date();
        };
        $scope.toggleMin();
        $scope.open = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.opened = true;
        };
        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };
        $scope.formats = ['dd-MMMM-yyyy','yyyy/MM/dd','dd.MM.yyyy','dd/MM/yyyy','shortDate'];
        $scope.format = $scope.formats[3];



        //-----Datepeaker End--------------------


    });
