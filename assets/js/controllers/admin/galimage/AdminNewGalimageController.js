angular.module('app')
  .controller('AdminNewGalimageController', function($scope,$http,$upload,Options) {
    $scope.alerts =[];

    $scope.imggallery = [];

    $scope.action='Add';
    $scope.title="Add New Image";


    $scope.galimage={}
    $scope.galimage.title = "";
    $scope.galimage.details = "";



    $http.get("imggallery").success(function(data){
      console.log(data);
      $scope.imggallery = data;
    });



    //file uploading
    //$scope.$watch('files', function () {
    //  $scope.upload($scope.files);
    //});

    $scope.upload = function (files) {
      $scope.files = files;
    };

    $scope.SaveGalimage = function(){
      if ($scope.files && $scope.files.length) {
        for (var i = 0; i < $scope.files.length; i++) {
          var file = $scope.files[i];
          console.log("take file");
          console.log($upload);
          $upload.upload({
            url: '/galimage/upload',
            fields: $scope.galimage,
            file: file
          }).progress(function (evt) {
            //  $("#profile-img-cont").empty().html('<i class="fa fa-refresh fa-spin"></i>');
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            console.log('progress: ' + progressPercentage + '% ' +
            evt.config.file.name);
          }).success(function (data, status, headers, config) {
            console.log('filename: ' + config.file.title + ' uploaded. Response: ' +
            JSON.stringify(data));
            $scope.image_path=data.data.image;
            $scope.alerts.push({ type: 'success', msg: 'Well done! You have successfully created a post( '+data.data.title+' ).' });
          });
        }
      }
    }

    $scope.addAlert = function() {
      $scope.alerts.push({msg: 'Another alert!'});
    };

    $scope.closeAlert = function(index) {
      $scope.alerts.splice(index, 1);
    };

  });
