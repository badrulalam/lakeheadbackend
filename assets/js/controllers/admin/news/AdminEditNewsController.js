/**
 * Created by ash on 6/14/15.
 */
angular.module('app')
    .controller('AdminEditNewsController', function($scope, $stateParams, uiGridConstants, $http,$state,Options) {

        $scope.alerts =[];
        $scope.action='Update';

        $scope.title="Edit News";

        $scope.news={}
        $scope.news.title = " ";
        $scope.news.description = " ";
        $scope.news.newsdate = "";
        $scope.news.slug = "";





        console.log($stateParams);


        $http.get('/news/'+ $stateParams.id).success (function(data){

            console.log(data);
            $scope.event={}

            $scope.news={}
            $scope.news.title = data.title;
            $scope.news.description = data.description;
            $scope.news.newsdate = data.newsdate;
            $scope.news.slug = data.slug;
            // $scope.blog.video_category=data.category.video_category;



        })





        $scope.MakeZeSlug = function(){
            $scope.news.slug = Options.convertToSlug( $scope.news.title);
        }

        $scope.UpdateNews = function(){
            var queryString = {"title":$scope.news.title,"description": $scope.news.description,"newsdate": $scope.news.newsdate,"slug": $scope.news.slug};

            $http.post('/news/'+$stateParams.id,queryString).

                success(function(data, status, headers, config) {
                    // this callback will be called asynchronously
                    // when the response is available
                    console.log("Event record Updated");
                    console.log(data);

                })

        }
    });
