angular.module('app')
    .controller('AdminNewNewsController', function($scope,$http,$state,Options) {

        $scope.alerts =[];
        $scope.action='Add';

        $scope.title="Add New News";

        $scope.news={}
        $scope.news.title = " ";
        $scope.news.description = " ";
        $scope.news.newsdate = "";
        $scope.news.slug = "";



        $scope.MakeZeSlug = function(){
            $scope.news.slug = Options.convertToSlug( $scope.news.title);
        }

        $scope.CreateNews = function(){

            var queryString = {"title":$scope.news.title,"description": $scope.news.description,"newsdate": $scope.news.newsdate,"slug": $scope.news.slug};

            $http.post("/news/create",queryString).

                success(function(data, status, headers, config) {
                    // this callback will be called asynchronously
                    // when the response is available
                    console.log("News record created")
                    console.log(data);
                    $scope.alerts.push({ type: 'success', msg: 'Well done! You have successfully created a News( '+data.title+' ).' });

                }).
                error(function(data, status, headers, config) {
                    $scope.alerts.push({ type: 'danger', msg: 'Oh snap! Change a few things up and try submitting again.' });
                });
        }

        $scope.addAlert = function() {
            $scope.alerts.push({msg: 'Another alert!'});
        };

        $scope.closeAlert = function(index) {
            $scope.alerts.splice(index, 1);
        };



    });
