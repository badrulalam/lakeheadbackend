/**
 * Created by ash on 6/14/15.
 */
angular.module('app')
    .controller('AdminEditPageController', function($scope, $stateParams, uiGridConstants, $http,$state,Options) {

        $scope.alerts =[];
        $scope.action='Update';

        $scope.title="Edit Page";

        $scope.page={}
        $scope.page.title = " ";
        $scope.page.body = " ";
        $scope.page.slug = "";





        console.log($stateParams);


        $http.get('/page/'+ $stateParams.id).success (function(data){

            console.log(data);
            $scope.event={}

            $scope.page={}
            $scope.page.title = data.title;
            $scope.page.body = data.body;
            $scope.page.slug = data.slug;
          })





        $scope.MakeZeSlug = function(){
            $scope.page.slug = Options.convertToSlug( $scope.page.title);
        }

        $scope.UpdatePage = function(){
            var queryString = {"title":$scope.page.title,"body": $scope.page.body,"slug": $scope.page.slug};

            $http.post('/page/'+$stateParams.id,queryString).

                success(function(data, status, headers, config) {
                    // this callback will be called asynchronously
                    // when the response is available
                    console.log("page record Updated");
                    console.log(data);

                })

        }
    });
