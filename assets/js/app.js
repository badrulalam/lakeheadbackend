angular.module('app', ['ui.bootstrap', 'ui.router','angularFileUpload', 'ngMessages', 'app-templates','ui.grid','ui.grid.edit','textAngular'])
  .run(function($rootScope, $state, Auth) {
    $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
      if (!Auth.authorize(toState.data.access)) {
        event.preventDefault();
        $state.go('anon.login');
      }
    });
  });