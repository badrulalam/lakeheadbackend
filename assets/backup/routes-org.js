angular.module('app')
  .config(function($stateProvider, $urlRouterProvider, AccessLevels) {

    $stateProvider
      .state('anon', {
        abstract: true,
        template: '<ui-view/>',
        data: {
          access: AccessLevels.anon
        }
      })
      //.state('anon.home', {
      //  url: '/',
      //  templateUrl: 'home.html'
      //})
      .state('anon.login', {
        url: '/login',
        templateUrl: 'auth/login.html',
        controller: 'LoginController'
      })
      .state('demo', {
        url: '/demo',
        templateUrl: 'user/demo.html',
        controller: 'DemoController'
      })
      .state('aboutus', {
        url: '/about-us',
        templateUrl: 'pages/about-us.html',
        controller: 'PagesController'
      })
      .state('services', {
        url: '/services',
        templateUrl: 'pages/services.html',
        controller: 'PagesController'
      })
      .state('anon.register', {
        url: '/register',
        templateUrl: 'auth/register.html',
        controller: 'RegisterController'
      });

    $stateProvider
      .state('user', {
        abstract: true,
        template: '<ui-view/>',
        data: {
          access: AccessLevels.user
        }
      })
      .state('user.messages', {
        url: '/messages',
        templateUrl: 'user/messages.html',
        controller: 'MessagesController'
      })

        .state('home', {
          url: '/',
          templateUrl: 'home.html',
          data: {
            access: AccessLevels.user
          }
        })
   //----------New ROute Start---------

        .state('admin',{
          url: '/admin',
          templateUrl: 'templates/admin/dashboard.html',
          controller: 'AdminDashboardController',
          data: {
            access: AccessLevels.admin
          }
        })


    //--------New ROute End------------



    ;

    $urlRouterProvider.otherwise('/');
  });