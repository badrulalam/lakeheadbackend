/**
* Event.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
    name: {
      type: 'string'
    },
    details: {
      type: 'string'
    },
    evtdate: {
      type:'integer',
      required:false
    },
    image: {
      type:'string',
      required:false
    },
    slug: {
      type: 'string'
    }
  }
};

