/**
* Imggallery.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
    name: {
      type: 'string'
    },
    sort: {
      type: 'integer'
    },
    image: {
      type: 'string'
    },
    galimages: {
      collection: 'galimage',
      via: 'img_gallery'
    }
  }
};

