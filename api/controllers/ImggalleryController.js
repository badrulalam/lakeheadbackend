/**
 * ImggalleryController
 *
 * @description :: Server-side logic for managing imggalleries
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    upload: function  (req, res) {
        if(req.method === 'GET')
            return res.json({'status':'GET not allowed'});
        // Call to /upload via GET is error

        //var uploadFile = req.file('uploadFile');
        //console.log(req.body.userId);
        var uploadFile = req.file('file');
        //console.log(uploadFile);

        uploadFile.upload({

            // You can apply a file upload limit (in bytes)
            maxBytes: 1000000,

            //dirname: require('path').resolve(sails.config.appPath, '/assets/images')
            dirname: sails.config.appPath+'/uploads/gallery/'

        },function onUploadComplete (err, files) {
            // Files will be uploaded to .tmp/uploads

            // IF ERROR Return and send 500 error with error
            if (err) return res.serverError(err);

            var baseAbsolutePath = process.cwd();
            var imagePath = files[0].fd.replace(baseAbsolutePath,'');

            //insert all profile images at database
            //var profileImgObj = {user_id:req.body.userId,img_path:imagePath}
            req.body.image = imagePath;
            //updated to database image path
            Galimage.create(req.body).exec(function afterwards(err,data,updated){
                if (err)
                    return res.serverError(err);
                console.log("new post created");
                res.json({status:200,data:data});
            });
        });
    }
};

